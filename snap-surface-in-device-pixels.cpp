#include <cmath>
#include <iostream>

static double my_round(double value, bool stable)
{
    if (!stable) {
        return std::round(value);
    } else {
        return std::round(value * 10) / 10;
    }
}

int main()
{
    /// The size of the decoration border.
    const double border = 1.0;

    /// Whether round() at .5 is hardened.
    const bool stable_round = false;

    /// Whether the window geomety is floored, i.e. simulate the real world case.
    const bool floor_xdg_geometry = true;

    for (double scale = 1.0; scale < 2; scale += 0.05) {
        for (double width = 5; width < 15; width += 0.05) {
            const int nativeWidth = my_round(width * scale, stable_round);
            const int nativeBorder = my_round(border * scale, stable_round);

            const int nativeClientWidth = nativeWidth - 2 * nativeBorder;
            double clientWidth = nativeClientWidth / scale;
            if (floor_xdg_geometry) {
                clientWidth = std::floor(clientWidth);
            }

            const int effectiveClientRight = nativeBorder + my_round(clientWidth * scale, stable_round);
            const int effectiveDecorationLeft = my_round((1 + clientWidth) * scale, stable_round);
            const int effectiveDecorationRight = my_round((1 + clientWidth + 1) * scale, stable_round);

            const bool collapsed = effectiveDecorationLeft == effectiveDecorationRight;
            if (effectiveClientRight != effectiveDecorationLeft || collapsed) {
                std::cout << "oops... " << width << " (@" << scale << ")" << " (" << (collapsed ? "collapsed" : effectiveClientRight < effectiveDecorationLeft ? "gap" : "occluded") << ")" << std::endl;
            }
        }
    }

    return 0;
}
