#include <cmath>
#include <fstream>
#include <iomanip>
#include <iostream>

static inline bool fuzzy_compare(double a, double b)
{
    return std::abs(a - b) * 1000000000000.0 <= std::min(std::abs(a), std::abs(b));
}

int main()
{
    std::ofstream file("stats.csv");
    file << "base,mismatches" << std::endl;

    for (int base = 10; base < 100; ++base) {
        int mismatch_count = 0;
        for (double scale = 1.0; scale <= 2.0; scale += 0.05) {
            const double scaled = base / scale;
            const double rounded = std::round(scaled);
            if (!fuzzy_compare(scaled, rounded)) {
                ++mismatch_count;
                // std::cout << std::setprecision(15) << std::fixed << scale << ": " << scaled << std::endl;
            }
        }
        file << base << "," << mismatch_count << std::endl;
    }

    return 0;
}
